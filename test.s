			# This code was produced by the CERI Compiler
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $5
	push $2
	pop %rbx
	pop %rax
	addq	%rax, %rbx
	push %rbx
	push $8
	push $6
	pop %rbx
	pop %rax
	addq	%rax, %rbx
	push %rbx
	pop %rbx
	pop %rax
	addq	%rax, %rbx
	push %rbx
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
